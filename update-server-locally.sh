#!/bin/bash

# Stop pm2 service
pm2 stop oursci-mango19

# Pull from repo
git pull

# NOTE: not building on server because memory limits
# install node modules in client
# cd client
# yarn install
# yarn build

# install node modules in webserver
cd server
yarn install

# Restart pm2 service
pm2 start oursci-mango19
