# Project Mango

Note: Mango serves mainly as a playground and reference project. You can find a [demo here](https://mango.our-sci.net).

Mango uses a MEVN stack (MongoDB, Express, Vue.js, Node.js).

## Quickstart

Start VueJS font-end on [http://localhost:8080](http://localhost:8080)

```
cd mango/client
yarn
yarn serve
```

Start Node.js back-end on [http://localhost:4010](http://localhost:4010)

```
cd mango/server
yarn
yarn start
```

Note: make sure a local mongodb server is running. See [client](./client/README.md) and [server](./server/README.md) READMEs for more information.

## Deploy to webserver

Mango is currently hosted at [https://mango.our-sci.net](https://mango.our-sci.net) on an AWS server.

```
cd mango
git commit -m "Changed a thing or two"
git push
# update client and server
./update-server.sh
# update server only
./update-server.sh -s
```

Note that you will need to have the "oursci-allround.pem" file in the root directory to update the webserver in this manner (oursci-allround.pem is not commited to git.)
