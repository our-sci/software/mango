#!/bin/bash

print_usage() {
  printf "Usage: $0 [ -s ]\n"
  printf "  -s   updates server module only (skip building and uploading client)\n"
}

server_only=0
while getopts 'sh' flag; do
  case "${flag}" in
    s) server_only=1 ;;
    *) print_usage
       exit 1 ;;
  esac
done


if [ $server_only -eq 0 ]; then
    # create build dist locally
    # because server does not have enough memory
    echo "Building production dist folder locally..."
    cd client
    yarn build
    cd ..
    echo "Transfer dist folder to server..."
    scp -i "oursci-allround.pem" -r client/dist/ ubuntu@ec2-52-207-218-148.compute-1.amazonaws.com:~/repos/mango/client/
fi

# run script on server to stop pm2 service, pull from repo, install and restart
ssh -i "oursci-allround.pem" ubuntu@ec2-52-207-218-148.compute-1.amazonaws.com << EOF
cd ~/repos/mango
bash -i ./update-server-locally.sh
EOF
