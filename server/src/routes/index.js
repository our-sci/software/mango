import debug from "./debug";

// TODO: create a helper function to create the routing code for the following entities
import surveys from "./surveys";
import surveyResults from "./surveyResults";
import auth from "./auth";
import users from "./users";
import admin from "./admin";
import groups from "./groups";

export default {
  debug,
  surveys,
  surveyResults,
  auth,
  users,
  admin,
  groups
};
