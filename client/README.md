# client

## Run locally

```
cd mango/client
yarn
yarn serve
```

This will start a local dev server on [http://localhost:8080](http://localhost:8080). To change the default port:

```
yarn serve --port 8090
```

## Run on webserver

Create a production build

```
cd mango/client
yarn
yarn build
```

This will generate a "dist" folder with a production build. The [server module](../server/README.md) will serve this folder statically.

### Inspect build

Run the following command to create a dist/report.html for inspecting the build bundle.

```
yarn build --report
```

## Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
