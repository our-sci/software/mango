const survey = {
  controls: [
    {
      name: "hello",
      label: "How do you say hello?",
      type: "inputText"
    },
    {
      name: "personal_stuff",
      label: "My group 2",
      type: "group",
      children: [
        {
          name: "text_0",
          label: "Enter some text 0",
          type: "inputText"
        },
        {
          name: "group_0",
          label: "My group 0",
          type: "group",
          children: [
            {
              name: "numeric_1",
              label: "Enter a number 1",
              type: "inputNumeric"
            }
          ]
        }
      ]
    }
  ],
  _id: "the-most-best-survey-2019",
  name: "the most best survey 2019",
  __v: 0
};

const getNumControls = (controls, countGroups = true) => {
  let n = 0;

  if (controls.length == 0) {
    return 0;
  }

  for (let i = 0; i < controls.length; i++) {
    if (controls[i].type == "group") {
      n = n + getNumControls(controls[i].children, countGroups);
      if (!countGroups) break;
    }
    n++;
  }

  return n;
};

const total = getNumControls(survey.controls, false);
console.log(total);

const getControl = (survey, position) => {
  let control;
  let { controls } = survey;
  position.forEach(i => {
    control = controls[i];
    controls = control.children;
  });

  return control;
};

let position = [1, 1, 0];

let c = getControl(survey, position);
//console.log(c);

const next = (survey, position) => {
  let { controls } = survey;
  let control = getControl(survey, position);

  if (
    control.type == "group" &&
    control.children &&
    control.children.length > 0
  ) {
    return [...position, 0];
  }

  let idx = position.pop();
  if (position == []) {
    // we are at the top
  }
  control = getControl(survey, position); // parent
};
