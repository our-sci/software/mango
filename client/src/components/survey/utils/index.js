import { unflatten } from "flat";
import _ from "lodash";

const survey = {
  _id: "5dadc4c9988f9df9527f07ac",
  name: "Generations Survey",
  controls: [
    {
      name: "name",
      label: "What is your name?",
      type: "inputText",
      options: {
        readOnly: false,
        required: false,
        relevance: "",
        constraint: "",
        calculate: ""
      }
    },
    {
      name: "age",
      label: "What is your age?",
      type: "inputNumeric",
      options: {
        readOnly: false,
        required: false,
        relevance: "",
        constraint: "",
        calculate: ""
      }
    },
    {
      name: "group_old",
      label: "Old group",
      type: "group",
      children: [
        {
          name: "medication",
          label: "What medication do you use?",
          type: "inputText",
          options: {
            readOnly: false,
            required: false,
            relevance: "",
            constraint: "",
            calculate: ""
          }
        },
        {
          name: "physical",
          label: "My group 2",
          type: "group",
          children: [
            {
              name: "vision",
              label: "What is your vision?",
              type: "inputNumeric",
              options: {
                readOnly: false,
                required: false,
                relevance: "",
                constraint: "",
                calculate: ""
              }
            },
            {
              name: "time_mile",
              label: "How long do run one mile?",
              type: "inputText",
              options: {
                readOnly: false,
                required: false,
                relevance: "",
                constraint: "",
                calculate: ""
              }
            }
          ],
          options: {
            readOnly: false,
            required: false,
            relevance: "",
            constraint: "",
            calculate: ""
          }
        }
      ],
      options: {
        readOnly: false,
        required: false,
        relevance: "",
        constraint: "",
        calculate: ""
      }
    },
    {
      name: "group_young",
      label: "My group 5",
      type: "group",
      children: [
        {
          name: "sports",
          label: "What sports do you watch?",
          type: "inputText",
          options: {
            readOnly: false,
            required: false,
            relevance: "",
            constraint: "",
            calculate: ""
          }
        },
        {
          name: "consoles",
          label: "Which consoles do you own?",
          type: "inputText",
          options: {
            readOnly: false,
            required: false,
            relevance: "",
            constraint: "",
            calculate: ""
          }
        }
      ],
      options: {
        readOnly: false,
        required: false,
        relevance: "",
        constraint: "",
        calculate: ""
      }
    },
    {
      name: "goodbye",
      label: "Say goodbye",
      type: "inputText",
      options: {
        readOnly: false,
        required: false,
        relevance: "",
        constraint: "",
        calculate: ""
      }
    }
  ]
};

const instance = {
  controls: [
    {
      name: "hello",
      label: "How do you say hello?",
      type: "inputText",
      value: "Grüezi"
    },
    {
      name: "personal_stuff",
      label: "My group 2",
      type: "group",
      children: [
        {
          name: "text_0",
          label: "Enter some text 0",
          type: "inputText",
          value: "This is completely random"
        },
        {
          name: "group_0",
          label: "My group 0",
          type: "group",
          children: [
            {
              name: "numeric_1",
              label: "Enter a number 1",
              type: "inputNumeric",
              value: 17
            }
          ]
        }
      ]
    }
  ],
  _id: "the-most-best-survey-2019",
  name: "the most best survey 2019",
  __v: 0
};

function* processSurveyNames(data) {
  if (!data) {
    return;
  }

  for (var i = 0; i < data.length; i++) {
    var val = data[i];
    yield val.name;

    if (val.children) {
      yield* processSurveyNames(val.children);
    }
  }
}

function* processSurveyNamesFull(data, namespace = "") {
  if (!data) {
    return;
  }

  for (var i = 0; i < data.length; i++) {
    var val = data[i];
    yield `${namespace == "" ? "" : namespace + "."}${val.name}`;

    if (val.children) {
      yield* processSurveyNamesFull(
        val.children,
        `${namespace == "" ? "" : namespace + "."}${val.name}`
      );
    }
  }
}

function* processJsDoc(data, namespace = "data") {
  if (!data) {
    return;
  }

  for (var i = 0; i < data.length; i++) {
    var val = data[i];

    let type = "any";
    switch (val.type) {
      case "group":
        type = "Object";
        break;
      case "inputNumeric":
        type = "number";
        break;
      case "inputText":
        type = "string";
        break;
      default:
        type = "any";
    }

    yield `@param {${type}} ${namespace == "" ? namespace : namespace + "."}${
      val.name
    }`;

    if (val.children) {
      yield* processJsDoc(
        val.children,
        `${namespace == "" ? namespace : namespace + "."}${val.name}`
      );
    }
  }
}

function* processPositions(data, position = []) {
  if (!data) {
    return;
  }

  for (var i = 0; i < data.length; i++) {
    var val = data[i];
    yield [...position, i];

    if (val.children) {
      yield* processPositions(val.children, [...position, i]);
    }
  }
}

// inspired by
// https://derickbailey.com/2015/07/19/using-es6-generators-to-recursively-traverse-a-nested-data-structure/
function* processData(data, namespace = "") {
  if (!data) {
    return;
  }

  for (var i = 0; i < data.length; i++) {
    var val = data[i];
    yield {
      [`${namespace == "" ? "" : namespace + "."}${val.name}`]: val.value
    };

    if (val.children) {
      yield* processData(
        val.children,
        `${namespace == "" ? "" : namespace + "."}${val.name}`
      );
    }
  }
}

/**
 * Returns a data object with keys and values from the instance controls.
 * e.g.
 *  instance = {controls: [{name: "msg", value: "hello"}, {name: "age", value: 30}], ...}
 *  =>
 *  instanceData = {msg: "hello", age: 30}
 * @param {Object} instance
 *
 * @returns {Object} Object with keys and values
 */
export const getInstanceData = instance => {
  let it = processData(instance.controls);
  let res = it.next();
  let objects = [];
  while (!res.done) {
    objects.push(res.value);
    res = it.next();
  }

  let c = Object.assign({}, ...objects);

  const u = unflatten(c, { safe: true });
  return u;
};

export const getSurveyPositions = survey => {
  let it = processPositions(survey.controls);
  let res = it.next();
  let positions = [];
  while (!res.done) {
    positions.push(res.value);
    res = it.next();
  }

  return positions;
};

export const getJsDoc = (survey, namespace = "data") => {
  let it = processJsDoc(survey.controls, namespace);
  let res = it.next();
  let annotations = [];
  while (!res.done) {
    annotations.push(res.value);
    res = it.next();
  }

  const doc = annotations.map(annotation => ` * ${annotation}`).join("\n");

  return `/**
 * @param {Object} ${namespace}
${doc}
*/`;
};

export const getAdvancedCodeTemplate = survey => {
  const doc = getJsDoc(survey);
  return `${doc}
function calculate(data) {
  return "";
}

${doc}
function showQuestion(data) {
  return true;
}`;
};

export const getControl = (survey, position) => {
  let control;
  let { controls } = survey;
  position.forEach(i => {
    control = controls[i];
    controls = control.children;
  });

  return control;
};

export const getBreadcrumbs = (survey, position) => {
  let breadcrumbs = [];
  let { controls } = survey;
  position.forEach(i => {
    breadcrumbs.push(controls[i].label);
    controls = controls[i].children;
  });

  return breadcrumbs;
};

// eslint-disable-next-line no-unused-vars
function has(target, key) {
  return true;
}

function get(target, key) {
  if (key === Symbol.unscopables) return undefined;
  return target[key];
}

export function compileSandboxSingleLine(src) {
  const wrappedSource = "with (sandbox) { return " + src + "}";
  const code = new Function("sandbox", wrappedSource);

  return function(sandbox) {
    const sandboxProxy = new Proxy(sandbox, { has, get });
    return code(sandboxProxy);
  };
}

export function compileSandbox(src, fname) {
  const wrappedSource =
    "with (sandbox) { " + src + "\nreturn " + fname + "(data); }";
  const code = new Function("sandbox", wrappedSource);

  return function(sandbox) {
    const sandboxProxy = new Proxy(sandbox, { has, get });
    return code(sandboxProxy);
  };
}

/**
 * Returns a new instance ready to upload
 * @param {Object} instance
 * @param {Object} survey
 *
 * @returns {Object} A representation of the instance ready for uploading to the server
 */
export const createInstancePayload = (instance, survey) => {
  const clone = _.cloneDeep(instance);
  const positions = getSurveyPositions(survey);

  positions.forEach(position => {
    const control = getControl(clone, position);
    delete control["label"];
    delete control["options"];
  });

  // rename "controls" to "data"
  // https://stackoverflow.com/a/50101979
  delete Object.assign(clone, { ["data"]: clone["controls"] })["controls"];

  clone["survey"] = survey._id;
  delete clone["_id"];
  delete clone["name"];
  clone["created"] = new Date();

  return clone;
};

export const handleize = str => {
  const handle = str && str.toLowerCase().replace(/\s/gi, "-");
  return handle;
};

// TOOD: decide how to do business logic on Survey and Survey instances
// Use a class? or helper function in here?
export const calculateControl = control => {
  if (!control.calculate || control.calculate === "") {
    return;
  }
};

const DBG = false;

if (DBG) {
  console.log();
  console.log("Processing instance:");
  const instanceData = getInstanceData(instance);
  console.log(instanceData);
}

if (DBG) {
  console.log();
  console.log("Pocessing names:");
  let it = processSurveyNames(survey.controls);
  let res = it.next();
  while (!res.done) {
    console.log(res.value);
    res = it.next();
  }
}

if (DBG) {
  console.log();
  console.log("Pocessing names full:");
  let it = processSurveyNamesFull(survey.controls);
  let res = it.next();
  while (!res.done) {
    console.log(res.value);
    res = it.next();
  }
}

if (DBG) {
  console.log();
  console.log("Processing positions:");
  let it = processPositions(survey.controls);
  let res = it.next();
  while (!res.done) {
    console.log(res.value);
    res = it.next();
  }
}

if (DBG) {
  console.log();
  console.log("Pocessing jsDoc:");
  let it = processJsDoc(survey.controls);
  let res = it.next();
  while (!res.done) {
    console.log(res.value);
    res = it.next();
  }
}
