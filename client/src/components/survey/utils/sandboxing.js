const data = {
  hello: "Grüezi",
  personal_stuff: {
    text_0: "This is completely random",
    group_0: { numeric_1: 17 }
  }
};

function compileCode(src) {
  src = "with (sandbox) { return " + src + "}";
  const code = new Function("sandbox", src);

  return function(sandbox) {
    const sandboxProxy = new Proxy(sandbox, { has, get });
    return code(sandboxProxy);
  };
}

function has(target, key) {
  return true;
}

function get(target, key) {
  if (key === Symbol.unscopables) return undefined;
  return target[key];
}

const sandbox = compileCode("data.personal_stuff.group_0.numeric_1");
let val = sandbox({ data });
console.log(val);
