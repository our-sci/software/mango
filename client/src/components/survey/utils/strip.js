import * as utils from "./index.js";
import { defaultControlProperties } from "./constants";
import _ from "lodash";

const survey = {
  _id: "calculating-survey-2019",
  controls: [
    {
      name: "first_name",
      label: "First name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate: ""
    },
    {
      name: "last_name",
      label: "Last name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate: ""
    },
    {
      name: "full_name",
      label: "Full name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate:
        "function calculate(data) { return 'newcalc ' + data.first_name + ' ' + data.last_name; }"
    }
  ],
  name: "Calculating Survey 2019",
  __v: 0
};
const instance = {
  _id: "calculating-survey-2019",
  controls: [
    {
      name: "first_name",
      label: "First name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate: "",
      value: "Andreas"
    },
    {
      name: "last_name",
      label: "Last name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate: "",
      value: "Rudolf"
    },
    {
      name: "full_name",
      label: "Full name",
      type: "inputText",
      readOnly: false,
      required: false,
      relevance: "",
      constraint: "",
      calculate:
        "function calculate(data) { return 'newcalc ' + data.first_name + ' ' + data.last_name; }",
      value: "Andreas Rudolf"
    }
  ],
  name: "Calculating Survey 2019",
  __v: 0
};
