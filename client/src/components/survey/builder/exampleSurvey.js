const controls = [
  {
    name: "hello",
    label: "How do you say hi?",
    type: "inputText"
  },
  {
    name: "random_number",
    label: "Pick a random number from 0 to 100",
    type: "inputNumeric"
  },
  {
    name: "another_group",
    label: "Task 5",
    type: "group",
    children: []
  },
  {
    name: "personal_group",
    label: "Personal Stuff",
    type: "group",
    children: [
      {
        name: "task_4",
        label: "Task 4"
      },
      {
        name: "task_2",
        label: "Task 2"
      }
    ]
  },
  {
    name: "clone",
    label: "How many clones would you like?",
    type: "inputNumeric"
  },
  {
    name: "clown",
    label: "Do you like clowns?",
    type: "inputBoolean"
  },
  {
    name: "color",
    label: "What is your favorite color?",
    type: "inputText"
  }
];

export default controls;
