import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import "./registerServiceWorker";

import api from "./services/api.service";
api.init(process.env.VUE_APP_API_URL);

import appRuler from "./components/ui/Ruler.vue";
import appFeedback from "./components/ui/Feedback.vue";
import appModal from "./components/ui/Modal.vue";
import appIcon from "./components/ui/Icon.vue";

Vue.component("appRuler", appRuler);
Vue.component("appFeedback", appFeedback);
Vue.component("appModal", appModal);
Vue.component("appIcon", appIcon);

Vue.filter("capitalize", function(value) {
  if (!value) return "";
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});

Vue.filter("showNull", function(value) {
  if (value === null) return "null";
  if (!value) return "";
  return value;
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
