import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Surveys from "./views/Surveys.vue";
import NotFound from "./views/NotFound.vue";

import SurveyBuilder from "./components/survey/builder/SurveyBuilder.vue";
import SurveyRunner from "./components/survey/runner/SurveyRunner.vue";
import SurveyResults from "./components/survey/results/SurveyResults.vue";

import Login from "./components/auth/Login.vue";
import Register from "./components/auth/Register.vue";
import Profile from "./components/auth/Profile.vue";

import Admin from "./views/admin/Admin.vue";
import UserList from "./views/users/UserList.vue";
import User from "./views/users/User.vue";
import UserEdit from "./views/users/UserEdit.vue";

import GroupList from "./views/groups/GroupList.vue";
import Group from "./views/groups/Group.vue";
import GroupEdit from "./views/groups/GroupEdit.vue";

import VModel from "./components/debug/VModel.vue";
import Monaco from "./components/debug/Monaco.vue";
import DebugVuex from "./components/debug/Vuex.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/surveys",
      name: "surveys",
      component: Surveys
    },
    {
      path: "/surveys/builder",
      name: "survey-builder-new",
      component: SurveyBuilder
    },
    {
      path: "/surveys/builder/edit/:id",
      name: "survey-builder-edit",
      component: SurveyBuilder
    },
    {
      path: "/surveys/run/:id",
      name: "survey-run",
      component: SurveyRunner
    },
    // survey results
    {
      path: "/survey-results/",
      name: "survey-results",
      component: SurveyResults
    },
    // Auth
    {
      path: "/auth/login",
      name: "auth-login",
      component: Login
    },
    {
      path: "/auth/register",
      name: "auth-register",
      component: Register
    },
    {
      path: "/auth/profile",
      name: "auth-profile",
      component: Profile
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin
    },
    // Users
    {
      path: "/users",
      name: "users",
      component: UserList
    },
    {
      path: "/users/new",
      name: "users-new",
      component: UserEdit
    },
    {
      path: "/users/edit/:id",
      name: "users-edit",
      component: UserEdit
    },
    {
      path: "/users/:id",
      name: "user-by-id",
      component: User
    },
    // Groups
    {
      path: "/groups",
      name: "group-list",
      component: GroupList
    },
    {
      path: "/groups/new",
      name: "group-new",
      component: GroupEdit
    },
    {
      path: "/groups/edit/:id",
      name: "group-edit",
      component: GroupEdit
    },
    {
      path: "/groups/*",
      name: "group-by-path",
      component: Group
    },
    // Debug
    {
      path: "/debug/vuex",
      name: "debug-vuex",
      component: DebugVuex
    },
    {
      path: "/debug/vmodel",
      name: "debug-vmodel",
      component: VModel
    },
    {
      path: "/debug/monaco",
      name: "debug-monaco",
      component: Monaco,
      props: route => ({
        initialCode: "// code here",
        ...route.params
      })
    },
    // Fallback
    {
      path: "*",
      name: "not-found",
      component: NotFound
    }
  ]
});
