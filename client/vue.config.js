// vue.config.js
const MonacoEditorPlugin = require("monaco-editor-webpack-plugin");

module.exports = {
  configureWebpack: {
    plugins: [
      new MonacoEditorPlugin({
        // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
        // Include a subset of languages support
        // TODO: figure out how to reduce bundle size
        // It is assumed all languages are loaded, since adding or leaving "typescript"
        // does not change bundle size:
        // yarn build --report
        // -> dist/report.html
        languages: ["javascript", "css", "html", "typescript"]
      })
    ]
  }
};
